import { Type } from 'class-transformer';
import { IsNotEmpty, ValidateNested } from 'class-validator';

class CreateChMatDetailDto {
  // @IsPositive()
  // @IsInt()
  @IsNotEmpty()
  materialId: number;

  // @Min(1)
  // @IsPositive()
  // @IsInt()
  @IsNotEmpty()
  quantity: number;

  // @IsNotEmpty()
  // lstQuantity: number;
}

export class CreateCheckMaterialDto {
  // @IsPositive()
  // @IsInt()
  @IsNotEmpty()
  employeeId: number;

  @Type(() => CreateChMatDetailDto)
  @ValidateNested({ each: true })
  checkItems: CreateChMatDetailDto[];
}
