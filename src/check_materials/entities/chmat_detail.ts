import { Material } from 'src/materials/entities/material.entity';
import {
  PrimaryGeneratedColumn,
  ManyToOne,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  Entity,
} from 'typeorm';
import { CheckMaterial } from './check_material.entity';

@Entity()
export class CheckMatDetail {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Material, (material) => material.checkItems)
  material: Material; //id

  @Column()
  name: string;
  @Column()
  last_quantity: number;
  @Column()
  quantity: number;

  @ManyToOne(() => CheckMaterial, (checkmat) => checkmat.checkItems)
  checkmat: CheckMaterial;

  @CreateDateColumn()
  createdDate: Date;
  @UpdateDateColumn()
  updatedDate: Date;
  @DeleteDateColumn()
  deletedDate: Date;
}
