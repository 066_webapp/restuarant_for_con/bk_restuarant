import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Category } from 'src/categories/entities/category.entity';
import { Repository } from 'typeorm';
import { CreateMenuDto } from './dto/create-menu.dto';
import { UpdateMenuDto } from './dto/update-menu.dto';
import { Menu } from './entities/menu.entity';

@Injectable()
export class MenusService {
  constructor(
    @InjectRepository(Menu)
    private menusRepository: Repository<Menu>,
    @InjectRepository(Category)
    private categorysRepository: Repository<Category>,
  ) {}

  async create(createMenuDto: CreateMenuDto) {
    const category = await this.categorysRepository.findOneBy({
      name: createMenuDto.categoryName,
    });
    const menu: Menu = new Menu();
    menu.name = createMenuDto.name;
    menu.price = createMenuDto.price;
    menu.status = createMenuDto.status;
    menu.category = category;
    menu.image = createMenuDto.image;
    return this.menusRepository.save(menu);
  }

  findAll() {
    return this.menusRepository.find({ relations: ['category'] });
  }

  async findOne(id: number) {
    const menu = await this.menusRepository.findOne({
      where: { id: id },
    });
    if (!menu) {
      throw new NotFoundException();
    }
    return menu;
  }

  async update(id: number, updateMenuDto: UpdateMenuDto) {
    const menu = await this.menusRepository.findOne({
      where: { id: id },
    });
    if (!menu) {
      throw new NotFoundException();
    }
    const category = await this.categorysRepository.findOneBy({
      name: updateMenuDto.categoryName,
    });
    const editMenu: Menu = new Menu();
    editMenu.name = updateMenuDto.name;
    editMenu.price = updateMenuDto.price;
    editMenu.status = updateMenuDto.status;
    editMenu.category = category;
    if (updateMenuDto.image == 'no-image.png' || updateMenuDto.image == '') {
      editMenu.image = menu.image;
    } else {
      editMenu.image = updateMenuDto.image;
    }

    const updatedMenu = { ...menu, ...editMenu };
    return this.menusRepository.save(updatedMenu);
  }

  async remove(id: number) {
    const menu = await this.menusRepository.findOne({
      where: { id: id },
    });
    if (!menu) {
      throw new NotFoundException();
    }
    return this.menusRepository.softRemove(menu);
  }
}
