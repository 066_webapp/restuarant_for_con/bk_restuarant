import { MenuQueue } from './../../menu_queues/entities/menu_queue.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Category } from 'src/categories/entities/category.entity';
import { ReceiptDetail } from 'src/receipts/entities/receipt-detail';

@Entity()
export class Menu {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true })
  name: string;

  @Column({ type: 'float' })
  price: number;

  @Column()
  status: string;

  @Column({
    length: '128',
    default: 'no-image.png',
  })
  image: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;

  @ManyToOne(() => Category, (category) => category.menus)
  category: Category;

  @OneToMany(() => MenuQueue, (menuQueue) => menuQueue.menu)
  menuQueues: MenuQueue[];

  @OneToMany(() => ReceiptDetail, (receiptDetail) => receiptDetail.menu)
  receiptDetail: ReceiptDetail[];
}
