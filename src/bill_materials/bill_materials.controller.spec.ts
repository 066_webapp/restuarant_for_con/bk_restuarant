import { Test, TestingModule } from '@nestjs/testing';
import { BillMaterialsController } from './bill_materials.controller';
import { BillMaterialsService } from './bill_materials.service';

describe('BillMaterialsController', () => {
  let controller: BillMaterialsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BillMaterialsController],
      providers: [BillMaterialsService],
    }).compile();

    controller = module.get<BillMaterialsController>(BillMaterialsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
