import { MenuQueue } from 'src/menu_queues/entities/menu_queue.entity';
import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateMenuQueueDto } from './dto/create-menu_queue.dto';
import { UpdateMenuQueueDto } from './dto/update-menu_queue.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Menu } from 'src/menus/entities/menu.entity';
import { Employee } from 'src/employees/entities/employee.entity';
import { Receipt } from 'src/receipts/entities/receipt.entity';
import { ReceiptDetail } from 'src/receipts/entities/receipt-detail';

@Injectable()
export class MenuQueuesService {
  constructor(
    @InjectRepository(MenuQueue)
    private menuQRepository: Repository<MenuQueue>,
    @InjectRepository(Menu)
    private menuRepository: Repository<Menu>,
    @InjectRepository(Receipt)
    private receiptRepository: Repository<Receipt>,
    @InjectRepository(Employee)
    private employeeRepository: Repository<Employee>,
    @InjectRepository(ReceiptDetail)
    private receiptDRepository: Repository<ReceiptDetail>,
  ) {}

  async create(createMenuQueueDto: CreateMenuQueueDto) {
    const menuQ = new MenuQueue();
    menuQ.name = createMenuQueueDto.name;
    menuQ.note = createMenuQueueDto.note;
    menuQ.status = createMenuQueueDto.status;
    menuQ.menu = await this.menuRepository.findOneBy({
      id: createMenuQueueDto.menuId,
    });
    menuQ.receipt = await this.receiptRepository.findOneBy({
      id: createMenuQueueDto.receiptId,
    });
    // menuQ.rec_id = createMenuQueueDto.receiptId;
    return this.menuQRepository.save(menuQ);
  }

  findAll() {
    return this.menuQRepository.find({
      relations: ['menu.category', 'chef', 'waitress', 'receipt.table'],
    });
  }

  async findOne(id: number) {
    const menuQ = await this.menuQRepository.findOne({
      where: { id: id },
      relations: ['menu.category', 'chef', 'waitress', 'receipt.table'],
    });
    if (!menuQ) {
      throw new NotFoundException();
    }
    return menuQ;
  }

  async update(id: number, updateMenuQueueDto: UpdateMenuQueueDto) {
    const menuQ0 = await this.menuQRepository.findOne({
      where: { id: id },
    });
    const menuQ = { ...menuQ0, ...updateMenuQueueDto };
    // menuQ.receipt = await this.receiptRepository.findOneBy({
    //   id: updateMenuQueueDto.receiptId,
    // });
    // console.log(menuQ);
    if (updateMenuQueueDto.chefId != undefined) {
      menuQ.chef = await this.employeeRepository.findOneBy({
        id: updateMenuQueueDto.chefId,
      });
    }
    if (updateMenuQueueDto.waitressId != undefined) {
      menuQ.waitress = await this.employeeRepository.findOneBy({
        id: updateMenuQueueDto.waitressId,
      });
    }
    if (!menuQ) {
      throw new NotFoundException();
    }
    return this.menuQRepository.save(menuQ);
  }

  async remove(id: number) {
    const menuQ = await this.menuQRepository.findOne({
      where: { id: id },
      relations: ['receipt', 'menu'],
    });
    if (!menuQ) {
      throw new NotFoundException();
    }
    const receipt = await this.receiptRepository.findOne({
      where: { id: menuQ.receipt.id },
      relations: ['receiptDetail.menu'],
    });
    for (const rcd of receipt.receiptDetail) {
      if (menuQ.menu.id == rcd.menu.id) {
        receipt.subtotal -= rcd.price;
        if (rcd.quantity > 1) {
          rcd.total -= rcd.price;
          rcd.quantity -= 1;
          await this.receiptDRepository.save(rcd);
        } else {
          rcd.total -= rcd.price;
          await this.receiptDRepository.softRemove(rcd);
        }
        receipt.total = receipt.subtotal - receipt.discount;
        await this.receiptRepository.save(receipt);
      }
    }
    menuQ.status = 'ยกเลิกรายการ';
    await this.menuQRepository.save(menuQ);
    return this.menuQRepository.softRemove(menuQ);
  }
}
