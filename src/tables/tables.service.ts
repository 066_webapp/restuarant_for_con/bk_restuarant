import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateTableDto } from './dto/create-table.dto';
import { UpdateTableDto } from './dto/update-table.dto';
import { Table } from './entities/table.entity';

@Injectable()
export class TablesService {
  constructor(
    @InjectRepository(Table) private tableRepository: Repository<Table>,
  ) {}
  create(createTableDto: CreateTableDto) {
    return this.tableRepository.save(createTableDto);
  }

  findAll() {
    return this.tableRepository.find();
  }

  async findOne(id: number) {
    const table = await this.tableRepository.findOne({ where: { id: id } });
    if (!table) {
      throw new NotFoundException();
    }
    return table;
  }

  async update(id: number, updateTableDto: UpdateTableDto) {
    const table = await this.tableRepository.findOneBy({ id: id });
    if (!table) {
      throw new NotFoundException();
    }
    const updateTable = { ...table, ...updateTableDto };
    return this.tableRepository.save(updateTable);
  }

  async remove(id: number) {
    const table = await this.tableRepository.findOneBy({ id: id });
    if (!table) {
      throw new NotFoundException();
    }
    return this.tableRepository.softRemove(table);
  }
}
