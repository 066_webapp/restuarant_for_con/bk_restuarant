import { Injectable } from '@nestjs/common';
// import { CreateReportDto } from './dto/create-report.dto';
// import { UpdateReportDto } from './dto/update-report.dto';
import { InjectDataSource } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';

@Injectable()
export class ReportsService {
  constructor(@InjectDataSource() private dataSource: DataSource) {}
  getMenu() {
    return this.dataSource.query('SELECT * FROM menu');
  }

  async getIncomesBy(startMonth: number, endMonth: number, year: string) {
    const listIncome: number[] = [];
    if (year == 'All Time') {
      const end: number = new Date().getFullYear();
      for (let i = 2020; i <= end; i++) {
        const rawQuery: any = await this.dataSource.query(
          'select ifnull(sum(receipt.total), 0) as income from receipt ' +
            `where receipt.status = 'ชำระเงินแล้ว' and YEAR(receipt.date) = '${i}' and receipt.deletedDate is null`,
          // `where receipt.status = 'ชำระเงินแล้ว' and strftime('%Y', receipt.date) = '${i}' and receipt.deletedDate is null`,
        );

        listIncome.push(rawQuery[0].income);
      }
      return listIncome;
    }
    if (startMonth == endMonth) {
      for (let i = 1; i <= 31; i++) {
        const txtDay = i < 10 ? `0${i}` : i;
        const txtMonth = startMonth < 10 ? `0${startMonth}` : startMonth;

        const rawQuery: any = await this.dataSource.query(
          'select ifnull(sum(receipt.total), 0) as income from receipt ' +
            `where receipt.status = 'ชำระเงินแล้ว' and DATE(receipt.date) = '${year}-${txtMonth}-${txtDay}' and receipt.deletedDate is null`,
          // `where receipt.status = 'ชำระเงินแล้ว' and strftime('%Y-%m-%d', receipt.date) = '${year}-${txtMonth}-${txtDay}' and receipt.deletedDate is null`,
        );

        listIncome.push(rawQuery[0].income);
      }
    } else {
      for (let i = startMonth; i <= endMonth; i++) {
        // const txtMonth = i < 10 ? `0${i}` : i;
        const txtMonth = i;

        const rawQuery: any = await this.dataSource.query(
          'select ifnull(sum(receipt.total), 0) as income from receipt ' +
            `where receipt.status = 'ชำระเงินแล้ว' and YEAR(receipt.date) = '${year}' and MONTH(receipt.date) = '${txtMonth}' and receipt.deletedDate is null`,
          // `where receipt.status = 'ชำระเงินแล้ว' and strftime('%Y-%m', receipt.date) = '${year}-${txtMonth}' and receipt.deletedDate is null`,
        );

        listIncome.push(rawQuery[0].income);
      }
    }
    return listIncome;
  }

  async getExpensesBy(startMonth: number, endMonth: number, year: string) {
    const listExpense: number[] = [];
    if (year == 'All Time') {
      const end: number = new Date().getFullYear();
      for (let i = 2020; i <= end; i++) {
        const rawQuery: any = await this.dataSource.query(
          'SELECT IFNULL(sum(bill_material.total),0) + (SELECT IFNULL(sum(salary.total),0) FROM salary ' +
            `WHERE YEAR(salary.date_salary) = '${i}' and salary.deletedAt is null) as expense FROM bill_material ` +
            `WHERE YEAR(bill_material.date) = '${i}' and bill_material.deletedDate is null`,
          // `WHERE strftime('%Y', salary.date_salary) = '${i}' and salary.deletedAt is null) as expense FROM bill_material ` +
          // `WHERE strftime('%Y', bill_material.date) = '${i}' and bill_material.deletedDate is null`,
        );

        listExpense.push(rawQuery[0].expense);
      }
      return listExpense;
    }
    if (startMonth == endMonth) {
      for (let i = 1; i <= 31; i++) {
        const txtDay = i < 10 ? `0${i}` : i;
        const txtMonth = startMonth < 10 ? `0${startMonth}` : startMonth;
        const rawQuery: any = await this.dataSource.query(
          'SELECT IFNULL(sum(bill_material.total),0) + (SELECT IFNULL(sum(salary.total),0) FROM salary ' +
            `WHERE DATE(salary.date_salary) = '${year}-${txtMonth}-${txtDay}' and salary.deletedAt is null) as expense FROM bill_material ` +
            `WHERE DATE(bill_material.date) = '${year}-${txtMonth}-${txtDay}' and bill_material.deletedDate is null`,
          // `WHERE strftime('%Y-%m-%d', salary.date_salary) = '${year}-${txtMonth}-${txtDay}' and salary.deletedAt is null) as expense FROM bill_material ` +
          // `WHERE strftime('%Y-%m-%d', bill_material.date) = '${year}-${txtMonth}-${txtDay}' and bill_material.deletedDate is null`,
        );
        listExpense.push(rawQuery[0].expense);
      }
    } else {
      for (let i = startMonth; i <= endMonth; i++) {
        // const txtMonth = i < 10 ? `0${i}` : i;
        const txtMonth = i;

        const rawQuery: any = await this.dataSource.query(
          'SELECT IFNULL(sum(bill_material.total),0) + (SELECT IFNULL(sum(salary.total),0) FROM salary ' +
            `WHERE YEAR(salary.date_salary) = '${year}' and MONTH(salary.date_salary) = '${txtMonth}' and salary.deletedAt is null) as expense FROM bill_material ` +
            `WHERE YEAR(bill_material.date) = '${year}' and MONTH(bill_material.date) = '${txtMonth}' and bill_material.deletedDate is null`,
          // `WHERE strftime('%Y-%m', salary.date_salary) = '${year}-${txtMonth}' and salary.deletedAt is null) as expense FROM bill_material ` +
          // `WHERE strftime('%Y-%m', bill_material.date) = '${year}-${txtMonth}' and bill_material.deletedDate is null`,
        );

        listExpense.push(rawQuery[0].expense);
      }
    }
    return listExpense;
  }

  getNearlyOut() {
    return this.dataSource.query(
      `SELECT * FROM material WHERE material.quantity < material.min_quantity;`,
    );
  }

  getPopFood() {
    return this.dataSource.query(
      'SELECT menu.categoryId, receipt_detail.name, menu.image ,count(receipt_detail.menuId) as pop_menu FROM receipt ' +
        'INNER JOIN receipt_detail ON receipt.id = receipt_detail.receiptId ' +
        'INNER JOIN menu ON receipt_detail.menuId = menu.id ' +
        'WHERE menu.categoryId != 6 and menu.categoryId != 7 ' +
        'GROUP BY receipt_detail.name ORDER BY pop_menu DESC',
    );
  }

  async getYearMaterials() {
    return this.dataSource.query(
      `SELECT DISTINCT YEAR(date) as Year FROM bill_material`,
      // `SELECT DISTINCT strftime('%Y', date) as Year FROM bill_material`,
    );
  }

  async getExpensesMaterialsBy(
    startMonth: number,
    endMonth: number,
    year: string,
  ) {
    const listExpensesM: number[] = [];
    if (year == 'All Time') {
      const end: number = new Date().getFullYear();
      for (let i = 2020; i <= end; i++) {
        const rawQuery: any = await this.dataSource.query(
          // `SELECT IFNULL(sum(bill_material.total),0) as expensesM FROM bill_material WHERE strftime('%Y', bill_material.date) = '${i}' and bill_material.deletedDate is null;
          `SELECT IFNULL(sum(bill_material.total),0) as expensesM FROM bill_material WHERE YEAR(bill_material.date) = '${i}' and bill_material.deletedDate is null;
          `,
        );

        listExpensesM.push(rawQuery[0].expensesM);
      }
      return listExpensesM;
    }
    if (startMonth == endMonth) {
      for (let i = 1; i <= 31; i++) {
        const txtDay = i < 10 ? `0${i}` : i;
        const txtMonth = startMonth < 10 ? `0${startMonth}` : startMonth;
        const rawQuery: any = await this.dataSource.query(
          // `SELECT IFNULL(sum(bill_material.total),0) as expensesM FROM bill_material WHERE strftime('%Y-%m-%d', bill_material.date) = '${year}-${txtMonth}-${txtDay}' and bill_material.deletedDate is null;
          `SELECT IFNULL(sum(bill_material.total),0) as expensesM FROM bill_material WHERE DATE(bill_material.date) = '${year}-${txtMonth}-${txtDay}' and bill_material.deletedDate is null;
          `,
        );
        listExpensesM.push(rawQuery[0].expensesM);
      }
    } else {
      for (let i = startMonth; i <= endMonth; i++) {
        // const txtMonth = i < 10 ? `0${i}` : i;
        const txtMonth = i;

        const rawQuery: any = await this.dataSource.query(
          // `SELECT IFNULL(sum(bill_material.total),0) as expensesM FROM bill_material WHERE strftime('%Y-%m', bill_material.date) = '${year}-${txtMonth}' and bill_material.deletedDate is null;
          `SELECT IFNULL(sum(bill_material.total),0) as expensesM FROM bill_material WHERE YEAR(bill_material.date) = '${year}' and MONTH(bill_material.date) = '${txtMonth}' and bill_material.deletedDate is null;
          `,
        );

        listExpensesM.push(rawQuery[0].expensesM);
      }
    }
    return listExpensesM;
  }
  async getSalaryEsBy(startMonth: number, endMonth: number, year: string) {
    const listSalaryE: number[] = [];
    if (year == 'All Time') {
      const end: number = new Date().getFullYear();
      for (let i = 2020; i <= end; i++) {
        const rawQuery: any = await this.dataSource.query(
          `SELECT IFNULL(sum(salary.total),0) AS salary_e FROM salary WHERE YEAR(salary.date_salary) = '${i}' and salary.deletedAt is null; `,
          // `SELECT IFNULL(sum(salary.total),0) AS salary_e FROM salary WHERE strftime('%Y', salary.date_salary) = '${i}' and salary.deletedAt is null; `,
        );

        listSalaryE.push(rawQuery[0].salary_e);
      }
      return listSalaryE;
    }
    if (startMonth == endMonth) {
      for (let i = 1; i <= 31; i++) {
        const txtDay = i < 10 ? `0${i}` : i;
        const txtMonth = startMonth < 10 ? `0${startMonth}` : startMonth;
        const rawQuery: any = await this.dataSource.query(
          `SELECT IFNULL(sum(salary.total),0) AS salary_e FROM salary WHERE DATE(salary.date_salary) = '${year}-${txtMonth}-${txtDay}' and salary.deletedAt is null; `,
          // `SELECT IFNULL(sum(salary.total),0) AS salary_e FROM salary WHERE strftime('%Y-%m-%d', salary.date_salary) = '${year}-${txtMonth}-${txtDay}' and salary.deletedAt is null; `,
        );
        listSalaryE.push(rawQuery[0].salary_e);
      }
    } else {
      for (let i = startMonth; i <= endMonth; i++) {
        const txtMonth = i;
        // const txtMonth = i < 10 ? `0${i}` : i;

        const rawQuery: any = await this.dataSource.query(
          // `SELECT IFNULL(sum(salary.total),0) AS salary_e FROM salary WHERE strftime('%Y-%m', salary.date_salary) = '${year}-${txtMonth}' and salary.deletedAt is null;`,
          `SELECT IFNULL(sum(salary.total),0) AS salary_e FROM salary WHERE YEAR(salary.date_salary) = '${year}' and MONTH(salary.date_salary) = '${txtMonth}' and salary.deletedAt is null;`,
        );

        listSalaryE.push(rawQuery[0].salary_e);
      }
    }
    return listSalaryE;
  }
  getPopBeverage() {
    return this.dataSource.query(
      'SELECT menu.categoryId, receipt_detail.name, menu.image ,count(receipt_detail.menuId) as pop_menu FROM receipt ' +
        'INNER JOIN receipt_detail ON receipt.id = receipt_detail.receiptId ' +
        'INNER JOIN menu ON receipt_detail.menuId = menu.id ' +
        'WHERE menu.categoryId = 7 ' +
        'GROUP BY receipt_detail.name ORDER BY pop_menu DESC',
    );
  }
  getPopDessert() {
    return this.dataSource.query(
      'SELECT menu.categoryId, receipt_detail.name, menu.image ,count(receipt_detail.menuId) as pop_menu FROM receipt ' +
        'INNER JOIN receipt_detail ON receipt.id = receipt_detail.receiptId ' +
        'INNER JOIN menu ON receipt_detail.menuId = menu.id ' +
        'WHERE menu.categoryId = 6 ' +
        'GROUP BY receipt_detail.name ORDER BY pop_menu DESC',
    );
  }

  // create(createReportDto: CreateReportDto) {
  //   return 'This action adds a new report';
  // }

  // findAll() {
  //   return `This action returns all reports`;
  // }

  // findOne(id: number) {
  //   return `This action returns a #${id} report`;
  // }

  // update(id: number, updateReportDto: UpdateReportDto) {
  //   return `This action updates a #${id} report`;
  // }

  // remove(id: number) {
  //   return `This action removes a #${id} report`;
  // }
}
